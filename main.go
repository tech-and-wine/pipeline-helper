package main

import (
	"fmt"
	"log"
	"os"

	"github.com/parakeet-nest/parakeet/completion"
	"github.com/parakeet-nest/parakeet/llm"
)

func main() {

	ollamaUrl := "http://localhost:11434"
	//model := "deepseek-coder"
	//model := "llama3"
	//model := "mistral:7b"
	//model := "phi3:medium"
	//model := "phi3:mini"
	
	model := "granite-code:3b" // 🤔
	//model := "codeqwen" // 🙂
	
	//model := "codegemma" // 😡
	//model := "deepseek-coder:6.7b" // 😡

	systemContent := `You are an helpful DevOps expert. 
	Help the user to generate a GitLab CI pipeline. 
	The output will be in YAML format.`

	// it would be better to give an example of job
	var contextContent = `<context>
	# Review apps:
	Provide an automatic live preview of changes made in a feature branch by spinning up a dynamic environment for your merge requests.
	Allow designers and product managers to see your changes without needing to check out your branch and run your changes in a sandbox environment.
	Are fully integrated with the GitLab DevOps LifeCycle.
	Allow you to deploy your changes wherever you want.

	## How review apps work
	A review app is a mapping of a branch with an environment. 
	Access to the review app is made available as a link on the merge request relevant to the branch.

	## Configuring review apps
	Review apps are built on dynamic environments, which allow you to dynamically create a new environment for each branch.

	The process of configuring review apps is as follows:

	- Set up the infrastructure to host and deploy the review apps (check the examples below).
	- Install and configure a runner to do deployment.
	- Set up a job in .gitlab-ci.yml that uses the predefined CI/CD variable ${CI_COMMIT_REF_SLUG} to create dynamic environments and 
	restrict it to run only on branches. 
	- Optionally, set a job that manually stops the review apps.
	</context>
	`

	userContent := `My CI pipeline is composed by three steps:
	1. building the java application
	2. testing the java application
	3. create a docker image from the java application
	4. pushing the docker image to the registry
	5. deploying the docker image to the Kubernetes

	Add jobs to deploy and undeploy review apps when doing merge requests (use the provided context for this part).
	`

	options := llm.Options{
		Temperature:   0.5, 
		RepeatLastN:   2,
		RepeatPenalty: 1.5,
	}

	query := llm.Query{
		Model: model,
		Messages: []llm.Message{
			{Role: "system", Content: systemContent},
			{Role: "system", Content: contextContent},
			{Role: "user", Content: userContent},
		},
		Options: options,
	}

	answer, chatErr := completion.ChatStream(ollamaUrl, query,
		func(answer llm.Answer) error {
			fmt.Print(answer.Message.Content)
			return nil
		})

	if chatErr != nil {
		log.Fatal("😡:", chatErr)
	}

	_ = os.WriteFile("result.md", []byte(answer.Message.Content), 0644)

}
