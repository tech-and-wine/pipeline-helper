# Review apps:
# Provide an automatic live preview of changes made in a feature branch by spinning up a dynamic environment for your merge requests.
# Allow designers and product managers to see your changes without needing to check out your branch and run your changes in a sandbox environment.
# Are fully integrated with the GitLab DevOps LifeCycle.
# Allow you to deploy your changes wherever you want.
# 1. build the java application
# 2. test the java application
# 3. create a docker image from the java application
# 4. push the docker image to the registry
# 5. deploy the docker image to the Kubernetes
# 6. undeploy the docker image from the Kubernetes
# 7. build and push the docker image to the registry and deploy it to the Kubernetes
# 8. undeploy the docker image from the Kubernetes
